FROM ruby:2.4.1
RUN wget -qO- https://deb.nodesource.com/setup_10.x | bash -
RUN apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle install
COPY . /myapp
